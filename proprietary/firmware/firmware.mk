PRODUCT_COPY_FILES += \
    vendor/xiaomi/sirius/proprietary/firmware-update/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/sirius/proprietary/firmware-update/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi/sirius/proprietary/firmware-update/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi/sirius/proprietary/firmware-update/dtbo.img:install/firmware-update/dtbo.img \
    vendor/xiaomi/sirius/proprietary/firmware-update/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/keymaster64.mbn:install/firmware-update/keymaster64.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/logfs_ufs_8mb.bin:install/firmware-update/logfs_ufs_8mb.bin \
    vendor/xiaomi/sirius/proprietary/firmware-update/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi/sirius/proprietary/firmware-update/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi/sirius/proprietary/firmware-update/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi/sirius/proprietary/firmware-update/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi/sirius/proprietary/firmware-update/vbmeta.img:install/firmware-update/vbmeta.img \
    vendor/xiaomi/sirius/proprietary/firmware-update/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi/sirius/proprietary/firmware-update/xbl_config.elf:install/firmware-update/xbl_config.elf
